.. VoxyPy documentation master file, created by
   sphinx-quickstart on Thu Nov 11 15:01:57 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

VoxyPy
==================================
**VoxyPy** is a data structure for operations on Voxel models and ``.vox`` files.

It uses `NumPy <https://pypi.org/project/numpy/>`_ for data storage and transformation.

Reading and writing ``.vox`` files is achieved using a heavily modified version of `numpy-vox-io <https://github.com/alexhunsley/numpy-vox-io>`_ for IO operations.

You can view the code, submit bugs/issues, and leave comments at the `VoxyPy GitLab repo <https://gitlab.com/Claytone/voxypy>`_.

.. note::
   This project is under active development.

.. include:: quickstart.rst

For complete documentation, see :ref:`voxel` and :ref:`entity`\ .

.. include:: voxel.rst

.. include:: entity.rst

Helpful Links
=============

- `VoxyPy GitLab repo <https://gitlab.com/Claytone/voxypy>`_
- `Claytone's blog for development commentary <https://claytonwells.com/>`_
- `NumPy <https://pypi.org/project/numpy/>`_
- `MagicaVoxel voxel editor <https://ephtracy.github.io/>`_

.. include:: changelog.rst

.. toctree::
   :maxdepth: 2
   :caption: Index

