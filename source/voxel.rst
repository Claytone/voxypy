.. _voxel:

Voxel
======

**Voxel** is a small class representing one cell in an Entity.
  
Voxel colors are represented as indices from 0 to 255, where 0 means blank. These indices refer to actual RGB colors in a 1x255 ``.png`` file. So, for example, if the 10th pixel in the texture is red, then a voxel with value 10 will render as red.

The Voxel equality operator works with ints and Voxels. Voxels can also be cast to ints.

Methods
---------

* ``_init__(color=0)`` - Voxel object with color **color**. 0 is blank.
    - **color** - 0-255
* ``get()`` - int value of voxel's ``color``.
* ``set(color=0)`` - Voxel object with color **color**. 0 is blank.
    - **color** - 0-255
* ``add(amount=1)`` - Adds ``amount`` to voxel's ``color``.
    - Adding 1 to 255 causes the color to wrap to 1 instead of 0. See `Voxel`_ for info about voxel colors.
* ``subtract(amount=1)`` - Subtracts ``amount`` from voxel's ``color``.
    - Just a wrapper for ``add``.
  

.. code-block:: python

    from voxypy.models import Voxel
    v1 = Voxel(1)
    v2 = Voxel(1)

    v1 == v2      # True
    v1 == 1       # True
    int(v1) == 1  # True
    v1 != 0       # True
