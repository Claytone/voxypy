.. _entity:

Entity
======

An Entity is (conceptually) a 3d array of :ref:`voxel`\ s.

Under the hood, an Entity is actually a wrapper for a 3d `NumPy ndarray <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`_ of ``int``\ s. Values are converted to :ref:`voxel`\ s during ``get`` and ``set`` methods. Voxels and ints can be used interchangeably in almost every case.


Initialization
--------------

.. code-block:: python

    import numpy as np
    from voxypy.models import Entity

    # Empty model with shape (1, 2, 3)
    entity = Entity(1, 2, 3)

    # Entity from numpy array
    data = np.array([1, 2, 3, 4]).reshape((2, 2, 1))
    entity = Entity(data=data) # Shape is automatically set from array

    # or with color palette
    palette = [(255, 0, 0, 255), (0, 255, 0, 255), (0, 0, 255, 255)]
    entity = Entity(data=data, palette=palette)

    # or from an existing .vox file
    entity = Entity().from_file('entity.vox')

Providing mismatched dimensions for ``x, y, z`` and ``data`` will throw an error.

Tricks
------

The methods ``get_dense`` and ``from_dense`` can be combined to allow invocation of any `NumPy ndarray method <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`_

.. code-block:: python

    entity = (x=10, y=10, z=10) # 10x10x10 Entity full of zeros.
    ones = entity.get_dense().fill(1)  # 10x10x10 ndarray full of ones.
    entity.from_dense(ones)  # 10x10x10 Entity full of ones.
    # one liner
    entity.from_dense(entity.get_dense().fill(1))

Methods
-------

- ``get(x, y, z)`` - A :ref:`voxel` object from Entity at ``[x, y, z]``
    - **x, y, z** - Coordinates.
- ``set(x, y, z, color)`` - Sets Entity at ``[x, y, z]`` to ``color`` and returns self.
    - **x, y, z** - Coordinates.
    - **color** - Either int(0-255) or a :ref:`voxel`\.
- ``get_dense()`` - Returns a `NumPy ndarray <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`_ of ``dtype=int``\ , where each int is a voxel color index.
- ``from_dense(dense)`` - Overwrites Entity voxels with new data. Does not overwrite palette.
    - **dense** - 3d NumPy ndarray with ``dtype=int``
- ``write(filename)`` - Writes entity to a .vox file.
    - **filename** - String name of a file.
- ``save(filename)`` - Wrapper for ``write()``
    - **filename** - String name of a file.
- ``from_file(filename)`` - Reads ``.vox`` file into an Entity, replacing existing data.
    - If the ``.vox`` file includes a color palette, it will also be read.
    - **filename** - String name of a file.
- ``layers()`` - Generator for list of z-layers.
- ``get_layer(z)`` - Returns layer at height ``z``.
- ``set_layer(z, layer)`` - Sets layer ``z`` of Entity to the contents of ``layer``. Dimensions must match.
    - **z** - Z level at which to insert **layer**.
    - **layer** - 2d `NumPy ndarray <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`_ with ``dtype=int``
- ``flip()`` - Flips the entity along the Z (gravity) axis.
- ``set_palette_from_file(filename)`` - Overwrites the entity's color palette with a new one from a png file.
    - **filename** - String name of a file.
- ``set_palette(palette)`` - Overwrites the entity's color palette with one that you pass in.
    - Palettes are validated and padded to fill missing color indices.
    - **palette** - List of 4-tuples (R, G, B, A=255)
- ``get_palette(padded=True)`` - Returns the entity's color palette in a list of tuples.
    - **padded** - If True, color at index 0 will be blank. This is handy because index 0 is interpreted as blank in voxel editors.
- ``nonzero()`` - Returns a list of tuples of the coordinates of the Entity which are not blank.
    - For example, ``[(0, 0, 0), (0, 0, 1) ... ]``