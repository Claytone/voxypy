Quickstart
----------

.. code-block:: Python

    from voxypy.models import Entity, Voxel
    dense = np.zeros((10, 10, 10), dtype=int)
    entity = Entity(data=dense)

    entity.set(x=1, y=2, z=3, 42)
    voxel = entity.get(1, 2, 3) # Voxel object with value 42

    voxel.add(1) # Voxel object with value 43
    new_voxel = Voxel(255)
    new_voxel.add(1) # Returns Voxel object with value 1
    entity.set(5, 5, 5, new_voxel)
    entity.set(5, 5, 5, 69)

    entity = Entity().from_file('old_entity.vox')
    # optional
    entity.set_palette_from_file('palette.png')
    entity.save('new_entity.vox')