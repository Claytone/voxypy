Change Log
==========

0.2.3
-----

- Added `nonzero()` to Entity

0.2.2
-----

- Fixed bad structure

0.2.1
-----

- Fixed missing ``imageio`` and ``Pillow`` requirements

0.2.0
-----

- Moved Entity and Voxel from ``voxypy.models.models`` to ``voxypy.models``
- Reworked palette behavior
- Removed ``palette_file`` from Entity
- Palette IO works with MagicaVoxel 0.99.6.4
- Added Sphinx/RTD docs at https://voxypy.readthedocs.io/en/latest/index.html
